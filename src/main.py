import telebot
from telebot import types
from email_sender import send_email

bot = telebot.TeleBot('1282822767:AAHuxJJcFtcY48KiHjxGHHU3Xa74IpnF5f4')

help_text = f'''
/help - выведет эту подсказку
/about - поделюсь небольшим рассказом о себе от GuVictory
/website - узнаешь, где можно найти сайт с моим портфолио
/vk - сможешь посетить мою страницу в вк
/projects - посмотреть исходный код проектов
/email - вместе отправим письмо на почту GuVictory
'''

subject = ''
email_text = ''


@bot.message_handler(commands=['website'])
def open_website(msg):
    makrup = types.InlineKeyboardMarkup()
    makrup.add(types.InlineKeyboardButton(
        'Посетить сайт с портфолио 🌝', url='https://guvictory.xyz'))
    bot.send_message(
        msg.chat.id, 'Узнайти про меня больше, просто перейдя на мой сайт!', parse_mode='html', reply_markup=makrup)


@bot.message_handler(commands=['vk'])
def open_vk(msg):
    makrup = types.InlineKeyboardMarkup()
    makrup.add(types.InlineKeyboardButton(
        'ТЫК ✌️', url='https://vk.com/vikcaifyst'))
    bot.send_message(
        msg.chat.id, 'Если у тебя есть ко мне какие-то срочные дела, просто напиши в ВК)', parse_mode='html', reply_markup=makrup)


@bot.message_handler(commands=['projects'])
def open_hub(msg):
    makrup = types.InlineKeyboardMarkup()
    makrup.add(types.InlineKeyboardButton(
        'GitHub 👾', url='https://github.com/GuVictory'))
    makrup.add(types.InlineKeyboardButton(
        'BitBucket 🤖', url='https://bitbucket.org/GuVictory'))
    bot.send_message(
        msg.chat.id, 'Посмотреть исходники некоторых моих работ можно на GitHub или BitBucket', parse_mode='html', reply_markup=makrup)


@bot.message_handler(commands=['about'])
def about(msg):
    makrup = types.InlineKeyboardMarkup()
    makrup.add(types.InlineKeyboardButton(
        'Посетить сайт с портфолио 👾', url='https://guvictory.xyz'))

    send_msg = f'''Привет, меня зовут Вика.
Вкратце о себе: люблю думать, учиться и общаться, много и с удовольствием работаю. Взамен жду интересных проектов и нового общения.
Сейчас я студентка 4 курса МГТУ им. Н. Э. Баумана по направлению «Математика и информатика».
В свободное время я путешествую и изучаю кулинарное искусство разных стран мира!\n
Советую вам перейти на мой сайт и ознакомиться с проектами и опытом!\n'''
    bot.send_message(msg.chat.id, send_msg,
                     parse_mode='html', reply_markup=makrup)


@bot.message_handler(commands=['help'])
def help(msg):
    bot.send_message(msg.chat.id, help_text, parse_mode='html')


@bot.message_handler(commands=['start'])
def start(msg):
    send_msg = f'<b>Привет, {msg.from_user.first_name}!</b>\nУзнай чем я могу помочь, просто напиши /help'
    bot.send_message(msg.chat.id, send_msg, parse_mode='html')


@bot.message_handler(commands=['email'])
def email(msg):
    send_msg = f'Я могу отправить GuVictory на почту сообщение от тебя, хочешь?'
    keyboard = types.InlineKeyboardMarkup()

    key_yes = types.InlineKeyboardButton(
        text='Да, хочу отправить', callback_data='key_yes')
    key_no = types.InlineKeyboardButton(
        text='Нет, передумал', callback_data='key_no')

    keyboard.add(key_yes)
    keyboard.add(key_no)

    bot.send_message(msg.chat.id, send_msg,
                     parse_mode='html', reply_markup=keyboard)


@bot.message_handler(content_types=['text'])
def message(msg):
    send_msg = f'''
    {msg.from_user.first_name}, мы конечно с GuVictory договаривались...
Но сейчас я бот, который не отличается умом и сообразительностью 😢\n
Tак что лучше напиши одну из команд из /help'''
    bot.send_message(msg.chat.id, send_msg, parse_mode='html')

# Обработчик нажатий на кнопки


@bot.callback_query_handler(func=lambda call: True)
def callback_worker(call):
    if call.data == "key_yes":
        msg = "Отлично, тогда сначала скажи мне тему сообщения:"
        bot.send_message(call.message.chat.id, msg)
        bot.register_next_step_handler(call.message, get_subject)
    if call.data == "key_no":
        msg = "Ну и ладно, тогда воспользуйся другими командами \help:"
        bot.send_message(call.message.chat.id, msg)


def get_subject(msg):
    global subject
    print(f'[SUBJECT]: {msg.text}')
    subject = msg.text
    bot.send_message(msg.from_user.id,
                     'Хорошо, а теперь напиши текст сообщения')
    bot.register_next_step_handler(msg, get_email_text)


def get_email_text(msg):
    global email_text
    email_text = msg.text
    print(f'[TEXT]: {msg.text}')
    bot.send_message(msg.from_user.id,
                     'Что мне подписать в поле от кого это сообщения, чтобы можно было дать обратную связь?')
    bot.register_next_step_handler(msg, get_sender)


def get_sender(msg):
    global email_text
    global subject
    sender = msg.text
    print(f'[SENDER]: {msg.text}')

    send_email(email_text, subject, sender)
    bot.send_message(msg.from_user.id,
                     'Все готово, писмо отправлено на почту GuVictory!')


@bot.message_handler(content_types=['sticker'])
def sticker_id(msg):
    bot.send_sticker(msg.chat.id, msg.sticker.file_id)


bot.polling(none_stop=True)
