import smtplib
from email.message import EmailMessage
from os import getenv
from pipenv.vendor.dotenv import load_dotenv, find_dotenv
from string import Template
from pathlib import Path

load_dotenv(find_dotenv())

html = Template(Path('./src/index.html').read_text())
EMAIL_LOGIN = getenv("email_login")
EMAIL_PASSWORD = getenv("email_password")


def send_email(text, subject, username):
    email = EmailMessage()
    email['from'] = f'GuVictoryBot: {username}'
    email['to'] = 'gubanova.vi10@gmail.com'
    email['subject'] = subject

    email.set_content(html.substitute(name=username, text=text), 'html')

    with smtplib.SMTP(host='smtp.gmail.com', port=587) as smtp:
        smtp.ehlo()
        smtp.starttls()
        # The login and password from the mail is stored in .env file
        print(EMAIL_LOGIN, EMAIL_PASSWORD)
        smtp.login(EMAIL_LOGIN, EMAIL_PASSWORD)
        smtp.send_message(email)
        print('Message was send!')
